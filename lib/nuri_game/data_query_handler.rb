require "nuri_game/data_query_handler/version"

# NuriGame namespace containing all the utility made by Nuri Yuri in order to make Games.
module NuriGame
  # Root class describing the basic data query handler to make data query in your game
  #
  # Usage :
  #   class YourDataQuery < NuriGame::DataQueryHandler
  #     def initialize(error_handler = nil)
  #       super(error_handler)
  #       # Do stuff here like @data = some_enumerable_data
  #     end
  #   # [...]
  #
  # You can custom #try2fetch_object and #handle_object_query to make more specific queries
  class DataQueryHandler
    # Create a new Data Query Handler
    # @param error_handler [Class, nil] the error handler to use to display the errors (should respond to #warning #error with parameters klass, message)
    # @param data [Enumerable] the preloaded data of the data query handler
    def initialize(error_handler = nil, data: nil)
      @sub_query_handlers = { self: nil }
      @data = data
      @error_handler = error_handler
    end
    
    # Function that invoke the query handler (recursively and stricly) in order to perform a data query
    #
    # When a query handle is not found but there's a object in the kwarg, the properties of the object are returned
    #
    # @example 
    #   Return the id, atk and dfe of a Monster object
    #     data_query_handle.query(:self, :id, :atk, :dfe, object: monster)
    #   Get a monster from the :monster query_handle
    #     data_query_handle.query(:monster, :self, id: monster_id)
    # @param handle [Symbol] name of the query handler (:self means were not requesting a sub query)
    # @param args [Array] list of argument to send to the query handler including the sub query handlers
    # @param kwarg [Hash<Symbol => Object>] list of keyword arguments to explain the query
    # @return [Object] result of the query
    def query(handle, *args, **kwarg)
      if (object = kwarg[:object])
        handle_object_query(object, args)
      elsif (query_handler = @sub_query_handlers[handle])
        query_handler.query(*args, **kwarg)
      elsif handle != :self
        raise_error(ArgumentError, 'Query Handler %<name>s not found!', name: handle)
      else
        perform_data_query(args, kwarg)
      end
    end
    
    # Function that invoke the query handler (recursively but not strictly) in order to perform a data query
    #
    # When a query handle is not found but there's a object in the kwarg, the properties of the object are returned
    #
    # @example 
    #   Return the id, atk and dfe of a Monster object
    #     data_query_handle.query!(:id, :atk, :dfe, object: monster)
    #   Get a monster from the :monster query_handle
    #     data_query_handle.query!(:monster, id: monster_id)
    # @param args [Array] list of argument to send to the query handler including the sub query handlers
    # @param kwarg [Hash<Symbol => Object>] list of keyword arguments to explain the query
    # @return [Object] result of the query
    def query!(*args, **kwarg)
      if (object = kwarg[:object])
        handle_object_query(object, args)
      elsif (query_handler = @sub_query_handlers[args.first])
        query_handler.query!(*args[1..-1], **kwarg)
      else
        perform_data_query(args, kwarg)
      end
    end
    
    # Function that adds a query handler to the sub_query_handlers
    # @param name [Symbol]
    # @param query_handler [DataQueryHandler]
    def add_query_handler(name, query_handler)
      raise_error(TypeError, 'Expected name to be a Symbol, got %<klass>s (%<value>s)', klass: name.class, value: name) unless name.is_a?(Symbol)
      raise_error(TypeError, 'Expected query_handler to be a DataQueryHandler, got %<klass>s', klass: query_handler.class) unless query_handler.is_a?(DataQueryHandler)
      raise_error(ArgumentError, 'Query Handler %<name>s already added.', name: name) if @sub_query_handlers.has_key?(name)
      @sub_query_handlers[name] = query_handler
    end
    
    private
    
    # Function that handle the property query on an object
    # @param object [Object]
    # @param properties [Array<Symbol>]
    # @return [Hash, object] if no properties, returns the object, otherwise, return a Hash associating the properties to the values
    def handle_object_query(object, properties)
      if properties.empty?
        object
      elsif properties.size == 1
        return object.public_send(properties.first)
      else
        hash = {}
        properties.each { |key| hash[key] = object.public_send(key) }
        hash
      end
    end
    
    # Function that perform a query on the data held by the query handle (this one will only be able to handle id query)
    # @param args [Array<Symbol>] list of properties
    # @param kwarg [Hash<Symbol => Object>] list of keyword used to find the object
    # @return [Object]
    def perform_data_query(args, kwarg)
      if @data && !@data.empty?
        return nil unless object = try2fetch_object(args, kwarg)
        return query(:self, *args, object: object)
      end
      raise_error(RuntimeError, 'Data is %<information>s.', information: @data ? 'empty' : 'not loaded')
    end
    
    # Function that tries to fetch the object from data
    # @param args [Array<Symbol>] list of properties
    # @param kwarg [Hash<Symbol => Object>] list of keyword used to find the object
    # @return [Object]
    def try2fetch_object(args, kwarg)
      if (id = kwarg[:id])
        unless (object = @data[id]).nil?
          object
        else
          raise_warning(id.is_a?(Integer) ? IndexError : KeyError, 'data[%<key>s] does not exist', key: id.inspect)
        end
      else
        keys = kwarg.keys.reject { |key| key == :id }.join(', ')
        raise_warning(KeyError, '%<keys>s couldn\'t be understood for this request', keys: keys)
      end
    end
    
    # Function that raise a warning with a specific class, message and the format of the message
    # @param klass [StandardError] the class to use to raise the error
    # @param message [String] the error message (unformated)
    # @param format_args [Array] the args to use in order to format message
    # @param format_kwarg [Hash] the kwargs to use in order to format message
    def raise_warning(klass, message, *format_args, **format_kwarg)
      message = format(message, *format_args, **format_kwarg)
      @error_handler ? @error_handler.warning(klass, message) : warn(format('[%<klass>s] : %<message>s', klass: klass, message: message))
    end
    
    # Function that raise an error with a specific class, message and the format of the message
    # @param klass [StandardError] the class to use to raise the error
    # @param message [String] the error message (unformated)
    # @param format_args [Array] the args to use in order to format message
    # @param format_kwarg [Hash] the kwargs to use in order to format message
    def raise_error(klass, message, *format_args, **format_kwarg)
      message = format(message, *format_args, **format_kwarg)
      @error_handler ? @error_handler.error(klass, message) : raise(klass, message)
    end
  end
end
