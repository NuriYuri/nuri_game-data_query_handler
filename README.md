# NuriGame::DataQueryHandler

A simple helper to make data queries on objects inside an Enumerable.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'nuri_game-data_query_handler'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install nuri_game-data_query_handler

## Usage

### Create your own DataQueryHandler

```ruby
require "nuri_game/data_query_handler" #< I'm not sure, it's my first gem

class YourDataQuery < NuriGame::DataQueryHandler
  def initialize(error_handler = nil)
    super(error_handler)
    # Do stuff here like @data = some_enumerable_data
  end
  
  # Overwrite handle_object_query or try2fetch_object if you want more specific queries
  # You can also overwrite query or query! to make sure your handler doesn't do unexpected things (like calling a sub handler if you don't want to).
end
```

### Use a DataQueryHandler

Lets assume dqh, dq2 and dq3 are data query handlers

#### Add dq2 & dq3 to dqh
```ruby
dqh.add_query_handler(:first_handler, dq2)
dqh.add_query_handler(:second_handler, dq3)
```
Note : You cannot add two DataQueryHandler on the same symbol, it'll raise ArgumentError with message "Query Handler <name> already added."

#### Get an object of dq2
```ruby
object = dqh.query(:first_handler, id: id_in_the_enum)
```
Note : When an object is not found, a IndexError (if id is a number) or a KeyError (otherwise) is raised.
Note2 : The enumerator cannot contain nil values, they're considered as non existing object since nil contain **no data**.

#### Get a property of an object of dq3
```ruby
property_value = dqh.query(:second_handler, :self, :property, id: id_in_the_enum)
# or
property_value = dqh.query!(:second_handler, :property, id: id_in_the_enum)
```
Note : query! doesn't raise an ArgumentError when doesn't find the query_handler :property, it does the same as telling :self before :property in query.

#### Get a hash of properties of an object of dq2
```ruby
hash = dqh.query(:first_handler, :self, :prop1, :prop2, :propn, id: id_in_the_enum)
# pr
hash = dqh.query!(:first_handler, :prop1, :prop2, :propn, id: id_in_the_enum)
```

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://gitlab.com/NuriYuri/nuri_game-data_query_handler.
