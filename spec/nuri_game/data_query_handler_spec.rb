RSpec.describe NuriGame::DataQueryHandler do
  it "has a version number" do
    expect(NuriGame::DataQueryHandler::VERSION).not_to be nil
  end
  
  $error_handler = Module.new do 
    
    module_function
    
    def warning(*args)
      raise *args
    end
  end
  
  it "handle_object_query and return the object" do
    dqh = NuriGame::DataQueryHandler.new
    obj = Object.new
    result = dqh.query(:self, object: obj)
    expect(result.__id__).to eq(obj.__id__)
  end
  
  it "handle_object_query and return the value of the property" do
    basic_data = Array.new(10) { rand(100) }
    dqh = NuriGame::DataQueryHandler.new(data: basic_data)
    index = rand(basic_data.size)
    result = dqh.query(:self, :next, object: basic_data[index])
    expect(result).to eq(basic_data[index].next)
  end
  
  it "handle_object_query and return the values of the property" do
    basic_data = Array.new(10) { rand(100) }
    dqh = NuriGame::DataQueryHandler.new(data: basic_data)
    index = rand(basic_data.size)
    result = dqh.query(:self, :next, :pred, object: basic_data[index])
    expected = { next: basic_data[index].next, pred: basic_data[index].pred }
    expect(result).to eq(expected)
  end
  
  it "perform_data_query" do
    basic_data = Array.new(10) { rand(100) }
    dqh = NuriGame::DataQueryHandler.new(data: basic_data)
    index = rand(basic_data.size)
    result = dqh.query(:self, :next, id: index)
    expect(result).to eq(basic_data[index].next)
  end
  
  it "raise RuntimeError (not loaded) during perform_data_query" do
    dqh = NuriGame::DataQueryHandler.new
    expect { dqh.query(:self, :next, id: 0) }.to raise_error(RuntimeError, 'Data is not loaded.')
  end
  
  it "raise RuntimeError (empty) during perform_data_query" do
    dqh = NuriGame::DataQueryHandler.new(data: [])
    expect { dqh.query(:self, :next, id: 0) }.to raise_error(RuntimeError, 'Data is empty.')
  end
  
  it "raise IndexError (out of bound, no properties) during perform_data_query" do
    dqh = NuriGame::DataQueryHandler.new($error_handler, data: [0])
    expect { dqh.query(:self, id: 1) }.to raise_error(IndexError, 'data[1] does not exist')
  end
  
  it "raise IndexError (out of bound?) during perform_data_query" do
    dqh = NuriGame::DataQueryHandler.new($error_handler, data: [0])
    expect { dqh.query(:self, :next, id: 1) }.to raise_error(IndexError, 'data[1] does not exist')
  end
  
  it "raise KeyError (doesn't exist in Hash) during perform_data_query" do
    dqh = NuriGame::DataQueryHandler.new($error_handler, data: { test: 0} )
    expect { dqh.query(:self, :next, id: :deploy) }.to raise_error(KeyError, 'data[:deploy] does not exist')
  end
  
  it "raise KeyError (Cannot understand query) during perform_data_query" do
    dqh = NuriGame::DataQueryHandler.new($error_handler, data: [0])
    expect { dqh.query(:self, :next, od: :deploy, sub_id: 0) }.to raise_error(KeyError, 'od, sub_id couldn\'t be understood for this request')
  end
  
  it "raise TypeError (not a Symbol) during add_query_handler" do
    dqh = NuriGame::DataQueryHandler.new
    expect { dqh.add_query_handler("test", nil) }.to raise_error(TypeError, 'Expected name to be a Symbol, got String (test)')
  end
  
  it "raise TypeError (not a DataQueryHandler) during add_query_handler" do
    dqh = NuriGame::DataQueryHandler.new
    expect { dqh.add_query_handler(:test, nil) }.to raise_error(TypeError, 'Expected query_handler to be a DataQueryHandler, got NilClass')
  end
  
  it "raise ArgumentError (already added) during add_query_handler" do
    dqh = NuriGame::DataQueryHandler.new
    dq2 = NuriGame::DataQueryHandler.new
    dq3 = NuriGame::DataQueryHandler.new
    dqh.add_query_handler(:test, dq2)
    expect { dqh.add_query_handler(:test, dq3) }.to raise_error(ArgumentError, 'Query Handler test already added.')
  end
  
  it "raise ArgumentError (not found) during query" do
    dqh = NuriGame::DataQueryHandler.new
    expect { dqh.query(:test) }.to raise_error(ArgumentError, 'Query Handler test not found!')
  end
  
  it "raise ArgumentError when sub query found but not the next one (query instead of query!)" do
    basic_data = Array.new(10) { rand(100) }
    dqh = NuriGame::DataQueryHandler.new(data: basic_data)
    dq2 = NuriGame::DataQueryHandler.new
    dq2.add_query_handler(:test, dqh)
    index = rand(basic_data.size)
    expect { dq2.query(:test, :next, id: index) }.to raise_error(ArgumentError, 'Query Handler next not found!')
  end
  
  it "perform_data_query of sub query using query!" do
    basic_data = Array.new(10) { rand(100) }
    dqh = NuriGame::DataQueryHandler.new(data: basic_data)
    dq2 = NuriGame::DataQueryHandler.new
    dq2.add_query_handler(:test, dqh)
    index = rand(basic_data.size)
    result = dq2.query!(:test, :next, id: index)
    expect(result).to eq(basic_data[index].next)
  end
  
  it "perform_data_query of sub query using query" do
    basic_data = Array.new(10) { rand(100) }
    dqh = NuriGame::DataQueryHandler.new(data: basic_data)
    dq2 = NuriGame::DataQueryHandler.new
    dq2.add_query_handler(:test, dqh)
    index = rand(basic_data.size)
    result = dq2.query(:test, :self, :next, id: index)
    expect(result).to eq(basic_data[index].next)
  end
  
end
